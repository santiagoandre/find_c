# FIND IN C
Un programa que busca archivos o directorios a partir de una direccion real o relativa siguiendo el estandar de comandos gnu/linux
## Compilar
Se genera un archivo binario llamado find
```
make
```
## Ejemplos de uso:
Buscar en la carpeta actual '.' el archivo llamado find.c
```
$  ./find  . find.c
path: .
name: find.c
./find.c
Total: 1
```
